<?php

namespace Drupal\mmaw_social_auth_provider\Authentication\Provider;

use Drupal\Core\PageCache\RequestPolicyInterface;
use Drupal\Core\Authentication\AuthenticationProviderInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\social_api\Plugin\NetworkManager;
use Drupal\social_auth\User\UserAuthenticator;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;
use Drupal\social_auth\SocialAuthDataHandler;
use Drupal\Core\Render\RendererInterface;
use Drupal\Component\Plugin\Exception\PluginException;
use League\OAuth2\Client\Token;

/**
 * Class SocialTokenAuth.
 */
class SocialTokenAuth  implements AuthenticationProviderInterface, RequestPolicyInterface{

  const AUTHMANAGERS = [
    'social_auth_facebook',
    'social_auth_google'
  ];

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\social_api\Plugin\pluginNetworkManager definition.
   *
   * @var \Drupal\social_api\Plugin\NetworkManager
   */
  protected $pluginNetworkManager;

  /**
   * Drupal\social_auth\User\socialAuthUserAuthenticator definition.
   *
   * @var \Drupal\social_auth\User\UserAuthenticator
   */
  protected $userAuthenticator;

  /**
   * Drupal\social_auth_facebook\FacebookAuthManager definition.
   *
   * @var \Drupal\social_api\AuthManager\OAuth2ManagerInterface
   */
  protected $socialAuthManager;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\social_auth\SocialAuthDataHandler definition.
   *
   * @var \Drupal\social_auth\SocialAuthDataHandler
   */
  protected $socialAuthDataHandler;

  /**
   * Drupal\Core\Render\RendererInterface definition.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;


  /**
   * Drupal\social_auth_google\GoogleAuthManager definition.
   *
   * @var string
   */
  protected $pluginId;

  /**
   * Constructs a new SocialTokenAuth object.
   */
  public function __construct(
    MessengerInterface $messenger,
    NetworkManager $plugin_network_manager,
    UserAuthenticator $social_auth_user_authenticator,
    RequestStack $request_stack,
    SocialAuthDataHandler $social_auth_data_handler,
    RendererInterface $renderer) {
    $this->messenger = $messenger;
    $this->pluginNetworkManager = $plugin_network_manager;
    $this->userAuthenticator = $social_auth_user_authenticator;
    $this->requestStack = $request_stack;
    $this->socialAuthDataHandler = $social_auth_data_handler;
    $this->renderer = $renderer;
    if (!$this->renderer) {
      $this->renderer = \Drupal::service('renderer');
    }

  }


  /**
   * {@inheritdoc}
   */
  public function applies(Request $request) {
    return (bool) $this->getRequestinfos($request);
  }


  /**
   * Gets a raw JsonWebToken from the current request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return array|bool
   *
   */
  protected function getRequestinfos(Request $request) {
    $has_token = FALSE;
    $is_social_token = FALSE;
    $auth = $request->headers->get('Authorization');
    $auth_type = $request->get('auth_type');

    if(in_array($this->pluginId, self::AUTHMANAGERS))
      $is_social_token = TRUE;

    if ($auth && preg_match('/^Bearer (.+)/', $auth, $matches)){
      $access_token = $matches[1];
      $has_token = TRUE;
    }
    if(!($has_token && $is_social_token))
      return FALSE;

    return ['token' => $access_token, 'auth_type' => $auth_type];
  }


  /**
   * Cache policy for pages served from social AUTH
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The incoming request object.
   *
   * @return string|null
   *   One of static::ALLOW, static::DENY or NULL.
   */
  public function check(Request $request) {
    if ($this->getRequestinfos($request)) {
      return RequestPolicyInterface::DENY;
    }
    return NULL;
  }


  /**
   * {@inheritdoc}
   */
  public function authenticate(Request $request) {
    // Sets the plugin id in user authenticator.
    $this->userAuthenticator->setPluginId($this->pluginId);

    // Sets the session prefix.
    $this->socialAuthDataHandler->setSessionPrefix($this->pluginId);

    // Sets the session keys to nullify if user could not logged in.
    $this->userAuthenticator->setSessionKeysToNullify(['access_token']);

    $infos = $this->getRequestinfos($request);
    $this->pluginId = $infos['auth_type'];
    $access_token = new Token\AccessToken(['access_token' => $infos['token']]);
    $container = \Drupal::getContainer();

    $this->socialAuthManager = $container->get($this->pluginId . '.manager');
    $this->socialAuthManager->setAccessToken($access_token);
    if(!$infos){
      $this->messenger->addError(t('Invalid Auth request.'));
      return NULL;
    }

    // Checks if there was an authentication error.
    $redirect = $this->checkAuthError();
    if ($redirect) {
      return NULL;
    }

    /* @var \League\OAuth2\Client\Provider\FacebookUser|null $profile */
    $profile = $this->processCallback();

    // If authentication was successful.
    if ($profile) {

      // Check for email.
      if (!$profile->getEmail()) {
        $this->messenger->addError(t('Authentication failed. This site requires permission to get your email address.'));

        return NULL;
      }

      // Gets (or not) extra initial data.
      $data = $this->userAuthenticator->checkProviderIsAssociated($profile->getId()) ? NULL : $this->socialAuthManager->getExtraDetails();

      // If user information could be retrieved.
      $this->userAuthenticator->authenticateUser($profile->getName(),
                                                 $profile->getEmail(),
                                                 $profile->getId(),
                                                 $this->socialAuthManager->getAccessToken(),
                                                 $profile->getPictureUrl(), $data);
    }

    return \Drupal::currentUser();

  }


  /**
   * Process implementer callback path.
   *
   * @return \League\OAuth2\Client\Provider\GenericResourceOwner|null
   *   The user info if successful.
   *   Null otherwise.
   */
  public function processCallback() {
    try {
      /* @var \League\OAuth2\Client\Provider\AbstractProvider|false $client */
      $client = $this->pluginNetworkManager->createInstance($this->pluginId)->getSdk();

      // If provider client could not be obtained.
      if (!$client) {
        $this->messenger->addError(t('auth_type not recognised'));
        return NULL;
      }
      $this->socialAuthManager->setClient($client)->authenticate();
      // Saves access token to session.
      $this->socialAuthDataHandler->set('access_token', $this->socialAuthManager->getAccessToken());

      // Gets user's info from provider.
      if (!$profile = $this->socialAuthManager->getUserInfo()) {
        $this->messenger->addError(t('Login failed, could not load user profile. Contact site administrator.'));
        return NULL;
      }
      return $profile;
    }
    catch (PluginException $exception) {
      $this->messenger->addError(t('There has been an error when creating plugin.'));

      return NULL;
    }
  }


  /**
   * Checks if there was an error during authentication with provider.
   *
   * When there is an authentication problem in a provider (e.g. user did not
   * authorize the app), a query to the client containing an error key is often
   * returned. This method checks for such key, dispatches an event, and returns
   * a redirect object where there is an authentication error.
   *
   * @param string $key
   *   The query parameter key to check for authentication error.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|boolean|null
   *   Redirect response object that may be returned by the controller or null.
   */
  protected function checkAuthError($key = 'error') {
    $request_query = $this->requestStack->getCurrentRequest()->query;

    // Checks if authentication failed.
    if ($request_query->has($key)) {
      $this->messenger->addError(t('You could not be authenticated.'));

      $response = $this->userAuthenticator->dispatchAuthenticationError($request_query->get($key));
      if ($response) {
        return $response;
      }

      return TRUE;
    }

    return NULL;
  }

}
